USE [RPA]
GO
/****** Object:  Table [dbo].[NIBSS]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS](
	[CHANNEL] [nvarchar](50) NULL,
	[SESSION_ID] [nvarchar](50) NULL,
	[NIBSS_REF_NO] [nvarchar](50) NULL,
	[TRANSACTION_TYPE] [nvarchar](50) NULL,
	[RESPONSE] [nvarchar](50) NULL,
	[AMOUNT] [nvarchar](50) NULL,
	[TRANSACTION_TIME] [nvarchar](50) NULL,
	[ORIGINATOR_BILLER] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NAME] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NO] [nvarchar](50) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[PAYMENT_REFERENCE] [nvarchar](500) NULL,
	[LAST_12_DIGITS_OF_SESSION_ID] [nvarchar](50) NULL,
	[PRODUCT] [nvarchar](50) NULL,
	[DIRECTION] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_COMPASS_INWARD]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_COMPASS_INWARD](
	[sessionid] [nvarchar](100) NULL,
	[accountnumber] [nvarchar](50) NULL,
	[accountname] [nvarchar](500) NULL,
	[amount] [decimal](18, 2) NULL,
	[requestdate] [date] NULL,
	[responsecode] [nvarchar](10) NULL,
	[trnstatus] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_COMPASS_OUTWARD]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_COMPASS_OUTWARD](
	[sessionid] [nvarchar](50) NULL,
	[trn_reference] [nvarchar](50) NULL,
	[FTSessionID] [nvarchar](50) NULL,
	[trn_maker] [nvarchar](100) NULL,
	[trn_debitacct] [nvarchar](50) NULL,
	[trn_creditacct] [nvarchar](50) NULL,
	[trn_amount] [decimal](18, 2) NULL,
	[trn_narration] [nvarchar](500) NULL,
	[requestdate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_EBILLS_CUSTOMER_TRAN]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_EBILLS_CUSTOMER_TRAN](
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[NIBSS_REF_NO] [nvarchar](50) NULL,
	[VALUE_DT] [date] NULL,
	[STMT_DT] [date] NULL,
	[USER_ID] [nvarchar](50) NULL,
	[AUTH_TIMESTAMP] [smalldatetime] NULL,
	[SAVE_TIMESTAMP] [smalldatetime] NULL,
	[AMOUNT] [decimal](20, 4) NULL,
	[ADDL_TEXT] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[AC_CCY] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_ECOBANK_SUCCESSFUL_TEMP]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_ECOBANK_SUCCESSFUL_TEMP](
	[CHANNEL] [nvarchar](500) NULL,
	[SESSION_ID] [nvarchar](200) NULL,
	[NIBSS_REF_NO] [nvarchar](200) NULL,
	[TRANSACTION_TYPE] [nvarchar](200) NULL,
	[RESPONSE] [nvarchar](200) NULL,
	[AMOUNT] [nvarchar](200) NULL,
	[TRANSACTION_TIME] [nvarchar](200) NULL,
	[ORIGINATOR_BILLER] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NAME] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NO] [nvarchar](200) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[PAYMENT_REFERENCE] [nvarchar](500) NULL,
	[LAST_12_DIGITS_OF_SESSION_ID] [nvarchar](50) NULL,
	[PRODUCT] [nvarchar](200) NULL,
	[DIRECTION] [nvarchar](50) NULL,
	[NIBSSSession] [int] NULL,
	[REF_MAN] [varchar](31) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_ESBDB]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_ESBDB](
	[POOL_ACCOUNT] [nvarchar](50) NULL,
	[SEND_ACCOUNT_NO] [nvarchar](50) NULL,
	[SENDER_NAME] [nvarchar](500) NULL,
	[AMOUNT] [decimal](18, 2) NULL,
	[REQUEST_DATE] [datetime] NULL,
	[TRANSACTION_REF_NO] [nvarchar](50) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[ACH_RSP_MSG] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_FAILED]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_FAILED](
	[CHANNEL] [nvarchar](100) NULL,
	[SESSION_ID] [nvarchar](100) NULL,
	[NIBSS_REF_NO] [nvarchar](100) NULL,
	[TRANSACTION_TYPE] [nvarchar](100) NULL,
	[RESPONSE] [nvarchar](100) NULL,
	[AMOUNT] [nvarchar](50) NULL,
	[TRANSACTION_TIME] [nvarchar](50) NULL,
	[ORIGINATOR_BILLER] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NAME] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NO] [nvarchar](50) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[PAYMENT_REFERENCE] [nvarchar](100) NULL,
	[LAST_12_DIGITS_OF_SESSION_ID] [nvarchar](50) NULL,
	[PRODUCT] [nvarchar](50) NULL,
	[DIRECTION] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_FLEXCUBE]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_FLEXCUBE](
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[NIBSS_REF_NO] [nvarchar](50) NULL,
	[VALUE_DT] [date] NULL,
	[STMT_DT] [date] NULL,
	[USER_ID] [nvarchar](50) NULL,
	[AUTH_TIMESTAMP] [smalldatetime] NULL,
	[SAVE_TIMESTAMP] [smalldatetime] NULL,
	[AMOUNT] [decimal](20, 4) NULL,
	[ADDL_TEXT] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[AC_CCY] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_POSTING]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_POSTING](
	[DETBSJRNL] [varchar](3) NULL,
	[BRN] [varchar](3) NULL,
	[BATCHNO] [varchar](1) NULL,
	[SRCCODE] [varchar](9) NULL,
	[AMOUNT] [decimal](38, 2) NULL,
	[ACNO] [varchar](10) NULL,
	[DRCR] [varchar](1) NULL,
	[AC_BRN] [varchar](3) NULL,
	[TXNCD] [varchar](3) NULL,
	[VALDT] [varchar](8) NULL,
	[INSTNO] [varchar](1) NULL,
	[SessionID] [varchar](5) NULL,
	[ADDLTEXT] [nvarchar](235) NULL,
	[COST_CENTER] [varchar](4) NULL,
	[reconDate] [varchar](11) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](500) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[VALUE_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[RRN] [nvarchar](500) NULL,
	[USER_ID] [nvarchar](500) NULL,
	[AMOUNT] [decimal](22, 4) NULL,
	[ADDL_TEXT] [nvarchar](3000) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[STATUS] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](500) NULL,
	[REF_MAN] [nvarchar](100) NULL,
 CONSTRAINT [PK_NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS] PRIMARY KEY CLUSTERED 
(
	[SN] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS_22102020]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS_22102020](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[VALUE_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[RRN] [nvarchar](500) NULL,
	[USER_ID] [nvarchar](50) NULL,
	[AMOUNT] [decimal](22, 4) NULL,
	[ADDL_TEXT] [nvarchar](500) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[STATUS] [nvarchar](50) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](50) NULL,
	[REF_MAN] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS_ARCHIVE]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS_ARCHIVE](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[VALUE_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[RRN] [nvarchar](500) NULL,
	[USER_ID] [nvarchar](500) NULL,
	[AMOUNT] [decimal](22, 4) NULL,
	[ADDL_TEXT] [nvarchar](max) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[STATUS] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](500) NULL,
	[REF_MAN] [nvarchar](100) NULL,
	[ReconDate] [varchar](10) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS_BASE]    Script Date: 3/31/2021 9:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_ACCOUNT_PROOFING_OPENITEMS_BASE](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[VALUE_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[RRN] [nvarchar](500) NULL,
	[USER_ID] [nvarchar](500) NULL,
	[AMOUNT] [decimal](22, 4) NULL,
	[ADDL_TEXT] [nvarchar](max) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[STATUS] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](50) NULL,
	[REF_MAN] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_MATCHEDITEMS]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_MATCHEDITEMS](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[VALUE_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[RRN] [nvarchar](500) NULL,
	[USER_ID] [nvarchar](500) NULL,
	[AMOUNT] [decimal](22, 4) NULL,
	[ADDL_TEXT] [nvarchar](max) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[STATUS] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](500) NULL,
	[REF_MAN] [nvarchar](100) NULL,
	[MATCHED_BY] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_MATCHEDITEMS_ARCHIVE]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_MATCHEDITEMS_ARCHIVE](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[VALUE_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](500) NULL,
	[RRN] [nvarchar](500) NULL,
	[USER_ID] [nvarchar](500) NULL,
	[AMOUNT] [decimal](22, 4) NULL,
	[ADDL_TEXT] [nvarchar](max) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[STATUS] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](500) NULL,
	[REF_MAN] [nvarchar](100) NULL,
	[MATCHED_BY] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_OPENITEMS]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_OPENITEMS](
	[AC_NO] [nvarchar](50) NULL,
	[TRN_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](50) NULL,
	[Narration] [nvarchar](500) NULL,
	[SAVE_TIMESTAMP] [smalldatetime] NULL,
	[DRCR_IND] [nvarchar](50) NULL,
	[Amount] [decimal](38, 4) NULL,
	[ReconDate] [varchar](10) NOT NULL,
	[NIBSS_Session] [int] NOT NULL,
	[Direction] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_RECOUP]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_RECOUP](
	[CHANNEL] [nvarchar](50) NULL,
	[SESSION_ID] [nvarchar](50) NULL,
	[NIBSS_REF_NO] [nvarchar](50) NULL,
	[TRANSACTION_TYPE] [nvarchar](50) NULL,
	[RESPONSE] [nvarchar](50) NULL,
	[AMOUNT] [nvarchar](50) NULL,
	[TRANSACTION_TIME] [nvarchar](50) NULL,
	[ORIGINATOR_BILLER] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NAME] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NO] [nvarchar](50) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[PAYMENT_REFERENCE] [nvarchar](500) NULL,
	[LAST_12_DIGITS_OF_SESSION_ID] [nvarchar](50) NULL,
	[PRODUCT] [nvarchar](50) NULL,
	[DIRECTION] [nvarchar](50) NULL,
	[ReconDate] [varchar](10) NOT NULL,
	[NIBSS_Session] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_REVERSAL]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_REVERSAL](
	[AC_NO] [nvarchar](50) NULL,
	[TRN_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](50) NULL,
	[Narration] [nvarchar](500) NULL,
	[SAVE_TIMESTAMP] [smalldatetime] NULL,
	[DRCR_IND] [nvarchar](50) NULL,
	[Amount] [decimal](38, 4) NULL,
	[ReconDate] [varchar](10) NOT NULL,
	[NIBSS_Session] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_RECON_UNIDENTIFIED_GL]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_RECON_UNIDENTIFIED_GL](
	[AC_NO] [nvarchar](50) NULL,
	[TRN_REF_NO] [nvarchar](50) NULL,
	[DRCR_IND] [nvarchar](10) NULL,
	[TRN_DT] [date] NULL,
	[EXTERNAL_REF_NO] [nvarchar](50) NULL,
	[NIBSS_REF_NO] [nvarchar](50) NULL,
	[VALUE_DT] [date] NULL,
	[STMT_DT] [date] NULL,
	[USER_ID] [nvarchar](50) NULL,
	[AUTH_TIMESTAMP] [smalldatetime] NULL,
	[SAVE_TIMESTAMP] [smalldatetime] NULL,
	[AMOUNT] [decimal](20, 4) NULL,
	[ADDL_TEXT] [nvarchar](500) NULL,
	[CARD_NO] [nvarchar](50) NULL,
	[AC_BRANCH] [nvarchar](50) NULL,
	[AC_CCY] [nvarchar](50) NULL,
	[COD_ACCT_NO] [nvarchar](50) NULL,
	[PRE_AUTH_SEQ_NO] [nvarchar](50) NULL,
	[ReconDate] [varchar](10) NOT NULL,
	[NIBSS_Session] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_SUCCESSFUL_ARCHIVE]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_SUCCESSFUL_ARCHIVE](
	[CHANNEL] [nvarchar](50) NULL,
	[SESSION_ID] [nvarchar](50) NULL,
	[NIBSS_REF_NO] [nvarchar](50) NULL,
	[TRANSACTION_TYPE] [nvarchar](50) NULL,
	[RESPONSE] [nvarchar](50) NULL,
	[AMOUNT] [nvarchar](50) NULL,
	[TRANSACTION_TIME] [nvarchar](50) NULL,
	[ORIGINATOR_BILLER] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NAME] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NO] [nvarchar](50) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[PAYMENT_REFERENCE] [nvarchar](500) NULL,
	[LAST_12_DIGITS_OF_SESSION_ID] [nvarchar](50) NULL,
	[PRODUCT] [nvarchar](50) NULL,
	[DIRECTION] [nvarchar](50) NULL,
	[NIBSSSession] [int] NULL,
	[ReconDate] [varchar](11) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBSS_SUCCESSFUL_TEMP]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBSS_SUCCESSFUL_TEMP](
	[CHANNEL] [nvarchar](500) NULL,
	[SESSION_ID] [nvarchar](200) NULL,
	[NIBSS_REF_NO] [nvarchar](200) NULL,
	[TRANSACTION_TYPE] [nvarchar](200) NULL,
	[RESPONSE] [nvarchar](200) NULL,
	[AMOUNT] [nvarchar](200) NULL,
	[TRANSACTION_TIME] [nvarchar](200) NULL,
	[ORIGINATOR_BILLER] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NAME] [nvarchar](500) NULL,
	[DESTINATION_ACCOUNT_NO] [nvarchar](200) NULL,
	[NARRATION] [nvarchar](500) NULL,
	[PAYMENT_REFERENCE] [nvarchar](500) NULL,
	[LAST_12_DIGITS_OF_SESSION_ID] [nvarchar](500) NULL,
	[PRODUCT] [nvarchar](200) NULL,
	[DIRECTION] [nvarchar](50) NULL,
	[NIBSSSession] [int] NULL,
	[REF_MAN] [varchar](31) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIP_SETTLEMENT_TEMP]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIP_SETTLEMENT_TEMP](
	[CHANNEL] [varchar](max) NULL,
	[AMOUNT] [varchar](max) NULL,
	[NARRATION] [varchar](max) NULL,
	[SETTLEMENTTYPE] [varchar](max) NULL,
	[SESSION] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIPATMDB_processed_transactions]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIPATMDB_processed_transactions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[stan] [varchar](50) NOT NULL,
	[terminal] [varchar](50) NOT NULL,
	[field37] [varchar](12) NOT NULL,
	[sourceaccount] [varchar](50) NULL,
	[sourceaccountname] [varchar](200) NULL,
	[destinationaccount] [varchar](50) NOT NULL,
	[destinationbank] [varchar](50) NOT NULL,
	[amount] [varchar](50) NULL,
	[flexcubereference] [varchar](50) NOT NULL,
	[flexcuberesponse] [varchar](50) NOT NULL,
	[ftsession_id] [varchar](30) NOT NULL,
	[nibssresponse] [varchar](50) NULL,
	[mti] [varchar](4) NOT NULL,
	[datetime] [datetime] NOT NULL,
	[datetimeprocessed] [datetime] NULL,
 CONSTRAINT [PK_NIPATMDB_processed_transactions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NipSettlementTracker]    Script Date: 3/31/2021 9:31:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NipSettlementTracker](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PRODUCT] [varchar](150) NULL,
	[SETTLEMENTDATE] [varchar](11) NULL,
	[SESSIONID] [varchar](5) NULL,
	[AMOUNT] [decimal](38, 2) NULL,
	[STATUS] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
