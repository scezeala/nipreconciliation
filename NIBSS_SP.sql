USE [RPA]
GO
/****** Object:  StoredProcedure [dbo].[sp_NIBSS_Inward_Reconciliation]    Script Date: 3/31/2021 9:33:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_NIBSS_Inward_Reconciliation]  
	@reconAccount varchar(15),
	@ebillsAccount varchar(15),
	@reconDate varchar(11),
	@nibssSession int,
	@startDateTime varchar(25),
	@endDateTime varchar(25)
AS
BEGIN
	SET NOCOUNT ON;

	truncate table NIBSS_RECON_RECOUP;
	truncate table NIBSS_RECON_REVERSAL;

	delete NIBSS where channel is null 
	or channel = 'Fund Transfer (Direct Credit)' or channel = 'CHANNEL'
	or channel = 'TRANSACTION TYPE'

    INSERT INTO NIBSS_RECON_RECOUP
	select inw.*, @reconDate as ReconDate, @nibssSession as NIBSS_Session from 
	(select * from NIBSS where direction = 'Inward') inw
	left join
	(select * from NIBSS_FLEXCUBE where ac_no = @reconAccount or ac_no = @ebillsAccount) flx
	on inw.SESSION_ID = flx.EXTERNAL_REF_NO
	where flx.EXTERNAL_REF_NO is null
	order by SESSION_ID
		

	INSERT INTO NIBSS_RECON_RECOUP
	select sl.*, @reconDate as ReconDate, @nibssSession as NIBSS_Session from 
	(select * from NIBSS where PRODUCT = 'EBILLSPAY' and DIRECTION = 'Inward') sl
	left join
	(select * from NIBSS_EBILLS_CUSTOMER_TRAN) gl
	on gl.EXTERNAL_REF_NO = sl.SESSION_ID  and gl.EXTERNAL_REF_NO <> ''
	where gl.EXTERNAL_REF_NO is null

	delete recp
	from
		NIBSS_RECON_RECOUP recp
		join NIBSS_COMPASS_INWARD inw
		on recp.session_id = inw.sessionid
	

	--INSERT INTO NIBSS_RECON_REVERSAL
	--select AC_NO, TRN_DT, EXTERNAL_REF_NO, ADDL_TEXT Narration, SAVE_TIMESTAMP, flx.AMOUNT, @reconDate as ReconDate, @nibssSession as NIBSS_Session from 
	--(select * from NIBSS_FLEXCUBE where AC_NO = @reconAccount) flx
	--left join
	--(select * from NIBSS where direction = 'Inward') inw
	--on flx.EXTERNAL_REF_NO = inw.SESSION_ID
	--where inw.SESSION_ID is null 
	--and (SAVE_TIMESTAMP >= @startDateTime and SAVE_TIMESTAMP < @endDateTime)
	--and USER_ID = 'COMPASS'
	--order by EXTERNAL_REF_NO
END
GO
/****** Object:  StoredProcedure [dbo].[sp_NIBSS_Outward_Reconciliation]    Script Date: 3/31/2021 9:33:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_NIBSS_Outward_Reconciliation] 
	@reconDate varchar(11),
	@nibssSession int,
	@startDateTime varchar(25),
	@endDateTime varchar(25),
	@userIDList varchar(500) = 'COMPASS, ECIBPLUS, ECOMOBILE, NIPSIM, OMNIFLOW'
AS
BEGIN
	SET NOCOUNT ON;

truncate table
   NIBSS_RECON_OPENITEMS;

delete NIBSS where channel is null 
	or channel = 'Fund Transfer (Direct Credit)' or channel = 'CHANNEL'
	or channel = 'TRANSACTION TYPE'

--delete
--    NIBSS_SUCCESSFUL_ARCHIVE
--where
--   ReconDate < @startDateTime

INSERT INTO
   NIBSS_RECON_RECOUP 
   select
      outw2.*,
      @reconDate as ReconDate,
      @nibssSession as NIBSS_Session 
   from
      (
         select
            outw1.* 
         from
            (
               select
                  outw.* 
               from
                  (
                     select
                        * 
                     from
                        NIBSS 
                     where
                        direction = 'Outward'
                  )
                  outw 
                  left join
                     (
                        select
                           * 
                        from
                           NIBSS_FLEXCUBE 
                     )
                     flx 
                     on flx.EXTERNAL_REF_NO = outw.NIBSS_REF_NO 
                     and flx.EXTERNAL_REF_NO <> '' 
               where
                  flx.EXTERNAL_REF_NO is null
            )
            outw1 
            left join
               (
                  select
                     * 
                  from
                     NIBSS_FLEXCUBE 
               )
               flx2 
               on flx2.EXTERNAL_REF_NO = outw1.SESSION_ID 
               and flx2.EXTERNAL_REF_NO <> '' 
         where
            flx2.EXTERNAL_REF_NO is null
      )
      outw2 
      left join
         (
            select
               * 
            from
               NIBSS_FLEXCUBE 
         )
         flx3 
         on flx3.NIBSS_REF_NO = outw2.SESSION_ID 
         and flx3.EXTERNAL_REF_NO <> '' 
   where
      flx3.EXTERNAL_REF_NO is null;
INSERT INTO
   NIBSS_RECON_OPENITEMS 
   select
      AC_NO,
      TRN_DT,
      EXTERNAL_REF_NO,
      ADDL_TEXT,
      SAVE_TIMESTAMP,
      DRCR_IND,
      Amount,
      @reconDate as ReconDate,
      @nibssSession as NIBSS_Session,
	  'Outward' as Direction 
   from
      NIBSS_FLEXCUBE 
   where
      EXTERNAL_REF_NO in 
      (
         select
            EXTERNAL_REF_NO 
         from
            (
               select
                  EXTERNAL_REF_NO,
                  SUM(
                  case
                     DRCR_IND 
                     when
                        'C' 
                     then
                        Amount 
                     when
                        'D' 
                     then
                        - 1 * Amount 
                  end
) Amount 
               from
                  (
                     select
                        flx1.* 
                     from
                        (
                           select
                              flx.* 
                           from
                              (
                                 select
                                    EXTERNAL_REF_NO,
                                    DRCR_IND,
                                    Amount 
                                 from
                                    NIBSS_FLEXCUBE 
                                 where
                                    EXTERNAL_REF_NO in 
                                    (
                                       select
                                          EXTERNAL_REF_NO 
                                       from
                                          NIBSS_FLEXCUBE 
                                       where
                                          SAVE_TIMESTAMP >= @startDateTime and
                                          AUTH_TIMESTAMP < @endDateTime 
                                          and AC_NO <> '151000009' 
                                          and USER_ID in 
                                          (
                                             select
                                                RTRIM(LTRIM(value)) 
                                             from
                                                string_split(@userIDList, ',')
                                          )
                                    )
                              )
                              flx 
                              left join
                                 (
                                    SELECT CHANNEL
										,SESSION_ID
										,NIBSS_REF_NO
										,TRANSACTION_TYPE
										,RESPONSE
										,AMOUNT
										,TRANSACTION_TIME
										,ORIGINATOR_BILLER
										,DESTINATION_ACCOUNT_NAME
										,DESTINATION_ACCOUNT_NO
										,NARRATION
										,PAYMENT_REFERENCE
										,LAST_12_DIGITS_OF_SESSION_ID
										,PRODUCT
										,DIRECTION
									FROM NIBSS 
                                    where
                                       DIRECTION = 'Outward' 
									union all
									SELECT CHANNEL
										,SESSION_ID
										,NIBSS_REF_NO
										,TRANSACTION_TYPE
										,RESPONSE
										,AMOUNT
										,TRANSACTION_TIME
										,ORIGINATOR_BILLER
										,DESTINATION_ACCOUNT_NAME
										,DESTINATION_ACCOUNT_NO
										,NARRATION
										,PAYMENT_REFERENCE
										,LAST_12_DIGITS_OF_SESSION_ID
										,PRODUCT
										,DIRECTION
									FROM NIBSS_SUCCESSFUL_ARCHIVE 
                                    where
                                       DIRECTION = 'Outward'									
                                 )
                                 outw 
                                 on flx.EXTERNAL_REF_NO = outw.SESSION_ID 
                           where
                              outw.SESSION_ID is null
                        )
                        flx1 
                        left join
                           (
                              SELECT CHANNEL
										,SESSION_ID
										,NIBSS_REF_NO
										,TRANSACTION_TYPE
										,RESPONSE
										,AMOUNT
										,TRANSACTION_TIME
										,ORIGINATOR_BILLER
										,DESTINATION_ACCOUNT_NAME
										,DESTINATION_ACCOUNT_NO
										,NARRATION
										,PAYMENT_REFERENCE
										,LAST_12_DIGITS_OF_SESSION_ID
										,PRODUCT
										,DIRECTION
									FROM NIBSS 
                                    where
                                       DIRECTION = 'Outward' 
									union all
									SELECT CHANNEL
										,SESSION_ID
										,NIBSS_REF_NO
										,TRANSACTION_TYPE
										,RESPONSE
										,AMOUNT
										,TRANSACTION_TIME
										,ORIGINATOR_BILLER
										,DESTINATION_ACCOUNT_NAME
										,DESTINATION_ACCOUNT_NO
										,NARRATION
										,PAYMENT_REFERENCE
										,LAST_12_DIGITS_OF_SESSION_ID
										,PRODUCT
										,DIRECTION
									FROM NIBSS_SUCCESSFUL_ARCHIVE 
                                    where
                                       DIRECTION = 'Outward' 
                           )
                           outw1 
                           on flx1.EXTERNAL_REF_NO = outw1.NIBSS_REF_NO 
                     where
                        outw1.NIBSS_REF_NO is null
                  )
                  d 
               where
                  EXTERNAL_REF_NO is not null 
               group by
                  EXTERNAL_REF_NO
            )
            al 
         where
            Amount > 0
      )
      and SAVE_TIMESTAMP >= @startDateTime 
      and AUTH_TIMESTAMP < @endDateTime 
   order by
      AUTH_TIMESTAMP;

--delete opn from 
--   NIBSS_RECON_OPENITEMS opn join NIBSS nbs on (opn.EXTERNAL_REF_NO = nbs.SESSION_ID or opn.EXTERNAL_REF_NO = nbs.NIBSS_REF_NO);

INSERT INTO
   NIBSS_RECON_REVERSAL 
   select
      AC_NO, TRN_DT, EXTERNAL_REF_NO, Narration, SAVE_TIMESTAMP, DRCR_IND, Amount, ReconDate, NIBSS_Session 
   from
	NIBSS_RECON_OPENITEMS 
   where
      EXTERNAL_REF_NO in 
      (
         select
            SESSION_ID 
         from
            NIBSS_FAILED 
         where
            direction = 'Outward' 
            and SESSION_ID <> '' 
         UNION ALL
         select
            NIBSS_REF_NO 
         from
            NIBSS_FAILED 
         where
            direction = 'Outward' 
            and NIBSS_REF_NO <> ''
      )
;
delete
   NIBSS_RECON_OPENITEMS 
where
   EXTERNAL_REF_NO in 
   (
      select
         SESSION_ID 
      from
         NIBSS_FAILED 
      where
         direction = 'Outward' 
         and SESSION_ID <> '' 
      UNION ALL
      select
         NIBSS_REF_NO 
      from
         NIBSS_FAILED 
      where
         direction = 'Outward' 
         and NIBSS_REF_NO <> ''
   )
;

INSERT INTO
   NIBSS_RECON_REVERSAL 
   select
      AC_NO, TRN_DT, EXTERNAL_REF_NO, Narration, SAVE_TIMESTAMP, DRCR_IND, Amount, ReconDate, NIBSS_Session 
   from
	NIBSS_RECON_OPENITEMS  
   where
      EXTERNAL_REF_NO in 
      (
         select
            flexcubereference 
         from
            NIPATMDB_processed_transactions         
         UNION ALL
         select
            ftsession_id 
         from
            NIPATMDB_processed_transactions          
      )
;
delete
   NIBSS_RECON_OPENITEMS 
where
   EXTERNAL_REF_NO in 
   (
      select
            flexcubereference 
         from
            NIPATMDB_processed_transactions         
         UNION ALL
         select
            ftsession_id 
         from
            NIPATMDB_processed_transactions
   )
;

INSERT INTO
   NIBSS_RECON_REVERSAL 
   select
      AC_NO, TRN_DT, EXTERNAL_REF_NO, Narration, SAVE_TIMESTAMP, DRCR_IND, Amount, ReconDate, NIBSS_Session 
   from
	NIBSS_RECON_OPENITEMS  
   where
      EXTERNAL_REF_NO in 
      (
         select
            TRANSACTION_REF_NO 
         from
            NIBSS_ESBDB
      )
;
delete
   NIBSS_RECON_OPENITEMS 
where
   EXTERNAL_REF_NO in 
   (
      select
            TRANSACTION_REF_NO 
         from
            NIBSS_ESBDB  
   )
;

INSERT INTO
   NIBSS_RECON_REVERSAL 
   select
      AC_NO, TRN_DT, EXTERNAL_REF_NO, Narration, SAVE_TIMESTAMP, DRCR_IND, Amount, ReconDate, NIBSS_Session 
   from
	NIBSS_RECON_OPENITEMS  
   where
      EXTERNAL_REF_NO in 
      (
         select sessionid from NIBSS_COMPASS_OUTWARD
		  union all
		  select trn_reference from NIBSS_COMPASS_OUTWARD
		  union all
		  select FTSessionID from NIBSS_COMPASS_OUTWARD
      )
;
delete
   NIBSS_RECON_OPENITEMS 
where
   EXTERNAL_REF_NO in 
   (
      select sessionid from NIBSS_COMPASS_OUTWARD
		union all
		select trn_reference from NIBSS_COMPASS_OUTWARD
		union all
		select FTSessionID from NIBSS_COMPASS_OUTWARD 
   )
;

INSERT INTO NIBSS_SUCCESSFUL_ARCHIVE SELECT *, @nibssSession, @reconDate FROM NIBSS
--INSERT INTO
--   NIBSS_RECON_UNIDENTIFIED_GL 
--   select
--      flx.*,
--      @reconDate as ReconDate,
--      @nibssSession as NIBSS_Session 
--   from
--      (
--         select
--            * 
--         from
--            NIBSS_FLEXCUBE 
--         where
--            SAVE_TIMESTAMP >= @startDateTime 
--            and AUTH_TIMESTAMP < @endDateTime 
--            and AC_NO <> '151000009' 
--            and USER_ID not in 
--            (
--               select
--                  RTRIM(LTRIM(value)) 
--               from
--                  string_split(@userIDList, ',')
--            )
--      )
--      flx 
--      left join
--         (
--            select
--               * 
--            from
--               NIBSS 
--            where
--               direction = 'Outward' 
--         )
--         outw 
--         on (flx.EXTERNAL_REF_NO = outw.SESSION_ID 
--         or flx.EXTERNAL_REF_NO = outw.NIBSS_REF_NO) 
--   where
--      (
--         outw.SESSION_ID is null 
--         and outw.NIBSS_REF_NO is null
--      )
--      and EXTERNAL_REF_NO is null;
END
GO
