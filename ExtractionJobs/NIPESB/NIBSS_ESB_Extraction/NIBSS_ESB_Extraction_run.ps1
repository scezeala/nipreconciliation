$fileDir = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $fileDir
java '-Dtalend.component.manager.m2.repository=%cd%/../lib' '-Xms256M' '-Xmx1024M' -cp '.;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/log4j-1.2.17.jar;../lib/mssql-jdbc.jar;../lib/ojdbc8-12.2.0.1.jar;nibss_esb_extraction_0_1.jar;' ecobank.nibss_esb_extraction_0_1.NIBSS_ESB_Extraction  --context=Default %*